<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','SiteController@index')->name('landing');
Route::get('/blog','BlogController@index')->name('blog.index');
Route::get('/blog/{slug}','BlogController@show');
Route::get('/team', function(){
	return view('team.index');
})->name('team.index');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
