@extends('layouts.main')

@section('content')
    <aside class="fh5co-page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="fh5co-page-heading-lead">
                        {{ config('app.name','Laravel') }} - TEAM
                        <span class="fh5co-border"></span>
                    </h1>
                    
                </div>
            </div>
        </div>
    </aside>

    <div id="fh5co-main">
        
        <div class="container">
            <div class="row">
                {{-- start content --}}
                <section class="team">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-10 col-md-offset-1">
                        <div class="col-lg-12">
                          <h6 class="description">OUR TEAM</h6>
                          <div class="row pt-md">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/1.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>Marrie Doi</h1>
                              <h2>Co-founder/ Operations</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/2.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>Christopher Di</h1>
                              <h2>Co-founder/ Projects</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/3.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>Heather H</h1>
                              <h2>Co-founder/ Marketing</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/4.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>John Doe</h1>
                              <h2>Designer</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/5.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>Peter John</h1>
                              <h2>Web Developer</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/6.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>Cherry John</h1>
                              <h2>Fullstack Developer</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/7.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>Frank Martin</h1>
                              <h2>Co-founder/ Operations</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/8.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>Christopher Di</h1>
                              <h2>Co-founder/ Projects</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/9.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>Heather H</h1>
                              <h2>Co-founder/ Marketing</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/10.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>Nancy Doe</h1>
                              <h2>Designer</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/11.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>Stella John</h1>
                              <h2>Web Developer</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                              <div class="img-box">
                                <img src="http://nabeel.co.in/files/bootsnipp/team/12.jpg" class="img-responsive">
                                <ul class="text-center">
                                  <a href="#"><li><span class="icon-instagram"></span></li></a>
                                </ul>
                              </div>
                              <h1>Cherry John</h1>
                              <h2>Fullstack Developer</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>          
            </div>
        </div>
    </div>
@endsection