@extends('layouts.main')

@section('content')

<div class="fh5co-slider">
    <div class="owl-carousel owl-carousel-fullwidth">
        @foreach($sliders as $item)
        <div class="item" style="background-image : url('/storage/{{ $item->image }}')">
            <div class="fh5co-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="fh5co-owl-text-wrap">
                            <div class="fh5co-owl-text text-center to-animate">
                                <h1 class="fh5co-lead">{{ $item->title }}</h1>
                                <h2 class="fh5co-sub-lead">{{ $item->content }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>	
<div id="fh5co-main">
    <!-- Features -->

    <div id="fh5co-features">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-8 col-md-offset-2">
                    <h2 class="fh5co-section-lead">Key Features</h2>
                    <h3 class="fh5co-section-sub-lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</h3>
                </div>
                <div class="fh5co-spacer fh5co-spacer-md"></div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 fh5co-feature-border">
                    <div class="fh5co-feature">
                        <div class="fh5co-feature-icon to-animate">
                            <i class="icon-bag"></i>
                        </div>
                        <div class="fh5co-feature-text">
                            <h3>Shopping Bag</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                            <p><a href="#">Read more</a></p>
                        </div>
                    </div>
                    {{--  <div class="fh5co-feature no-border">
                        <div class="fh5co-feature-icon to-animate">
                            <i class="icon-head"></i>
                        </div>
                        <div class="fh5co-feature-text">
                            <h3>User Satisfaction</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                            <p><a href="#">Read more</a></p>
                        </div>
                    </div>  --}}
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="fh5co-feature">
                        <div class="fh5co-feature-icon to-animate">
                            <i class="icon-microphone"></i>
                        </div>
                        <div class="fh5co-feature-text">
                            <h3>Voice Recording</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                            <p><a href="#">Read more</a></p>
                        </div>
                    </div>
                    {{--  <div class="fh5co-feature no-border">
                        <div class="fh5co-feature-icon to-animate">
                            <i class="icon-clock2"></i>
                        </div>
                        <div class="fh5co-feature-text">
                            <h3>24/7 Support</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                            <p><a href="#">Read more</a></p>
                        </div>
                    </div>  --}}
                </div>
            </div>
        </div>
    </div>
    <!-- Features -->


    <div class="fh5co-spacer fh5co-spacer-lg"></div>		
    <!-- Products -->
    <div class="container" id="fh5co-products">
        <div class="row text-left">
            <div class="col-md-8">
                <h2 class="fh5co-section-lead">Products</h2>
                <h3 class="fh5co-section-sub-lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</h3>
            </div>
            <div class="fh5co-spacer fh5co-spacer-md"></div>
        </div>
        <div class="row">
            @foreach($posts as $item)
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
                    <div class="fh5co-product">
                        <img src="{{ url('/storage/'.$item->image) }}" alt="Post Image" class="img-responsive img-rounded to-animate">
                        <h4>{{ $item->title }}</h4>
                        <p>{{ str_limit( $item->excerpt, 100) }}</p>
                        <p><a href="{{ url('/blog/'.$item->slug) }}">Read more</a></p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <!-- Products -->
    <div class="fh5co-spacer fh5co-spacer-lg"></div>

    {{--  <div id="fh5co-clients">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="images/client_1.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="images/client_2.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
                <div class="visible-sm-block visible-xs-block clearfix"></div>
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="images/client_3.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="images/client_4.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
            </div>
        </div>
    </div>  --}}

    <div class="fh5co-bg-section" style="background-image: url(images/slide_2.jpg); background-attachment: fixed;">
        <div class="fh5co-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="fh5co-hero-wrap">
                        <div class="fh5co-hero-intro text-center">
                            <h1 class="fh5co-lead"><span class="quo">&ldquo;</span>Design is not just what it looks like and feels like. Design is how it works. <span class="quo">&rdquo;</span></h1>
                            <p class="author">&mdash; <cite>Steve Jobs</cite></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection