@extends('layouts.main')

@section('content')
    <aside class="fh5co-page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="fh5co-page-heading-lead">
                        {{ config('app.name','Laravel') }} - Blog
                        <span class="fh5co-border"></span>
                    </h1>
                    
                </div>
            </div>
        </div>
    </aside>

    <div id="fh5co-main">
        
        <div class="container">
            <div class="row">
                {{-- start content --}}
                <div class="col-md-8">
                  @foreach ($posts as $item)
                    <div class="panel panel-default">
                      <img class="img img-responsive" src="{{ url('/storage/'.$item->image) }}" alt="Card image cap">
                      <div class="panel-body">
                        <h3 class="card-title">{{$item->title}}</h3>
                            <p> {!! str_limit($item->body, 400) !!} </p>
                            <a href="{{ url('/blog/'.$item->slug) }}" class="btn btn-primary btn-sm">Read More</a>
                      </div>
                      <div class="panel-footer">
                        <span class="icon-calendar"></span> {{ $item->created_at->format('F j, Y') }} | <span class="icon-user"></span> {{$item->author->name}}
                      </div>
                    </div>
                  @endforeach
                  {{$posts->links()}}
                </div>
                {{-- end content --}}
                
                {{-- start sidebar --}}
                <div class="col-md-4">
                    <div class="fh5co-sidebox">
                        <h3 class="fh5co-sidebox-lead">Yang Baru di BLog {{ config('app.name','Laravel') }}</h3>	
                        <ul class="fh5co-post">
                            @foreach($newposts as $item)
                            <li>
                                <a href="{{ url('/blog/'.$item->slug) }}">
                                    <div class="fh5co-post-media"><img src="{{ url('/storage/'.$item->image) }}" alt="Post Image"></div>
                                    <div class="fh5co-post-blurb">
                                        <h4>{{ $item->title }}</h4>
                                        <p>{{ str_limit( $item->excerpt, 50) }}</p>
                                        <span class="fh5co-post-meta">{{ $item->created_at->format('M d, Y') }}</span>
                                    </div>
                                </a>
                            </li>
                            @endforeach
                        </ul>                     
                    </div>

                    <div class="fh5co-sidebox">
                        <h3 class="fh5co-sidebox-lead">Paragraph</h3>	
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, temporibus vitae. Dolores sequi, animi dolorem. Ullam minima laudantium culpa dolorem, nulla doloribus totam obcaecati reprehenderit quasi nam eius autem nihil.</p>
                    </div>

                    <div class="fh5co-sidebox">
                        <h3 class="fh5co-sidebox-lead">Check list</h3>	
                        <ul class="fh5co-list-check">
                            <li>Lorem ipsum dolor sit.</li>
                            <li>Nostrum eveniet animi sint.</li>
                            <li>Dolore eligendi, porro ipsam.</li>
                            <li>Repudiandae voluptate dolorem voluptas.</li>
                            <li>Voluptate cupiditate, est laborum?</li>
                        </ul>
                    </div>
                </div>
                {{-- end sidebar --}}
                
            </div>
        </div>
    </div>
@endsection