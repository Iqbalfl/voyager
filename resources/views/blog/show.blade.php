@extends('layouts.main')

@section('content')
    <aside class="fh5co-page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="fh5co-page-heading-lead">
                        {{ config('app.name','Laravel') }} - Blog
                        <span class="fh5co-border"></span>
                    </h1>
                    
                </div>
            </div>
        </div>
    </aside>

    <div id="fh5co-main">
        
        <div class="container">
            <div class="row">
                {{-- start content --}}
                <div class="col-md-8">
                    <h2>{!! $post->title !!}</h2>
                    <div class="info-meta">
                        <ul class="list-inline">
                            <li><span class="icon-user"></span> {{ $post->created_at->format('F j, Y') }}</li>
                            <li><span class="icon-calendar"></span> Posted by {{$post->author->name}}</li>
                            <li><span class="icon-tag"></span> {{$post->category->name}}</li>
                        </ul>
                    </div>
                    <div class = "media">
                        <div class = "media-body">
                            <img src="{{ url('/storage/'.$post->image) }}" alt="Post Image" class="img-responsive img-rounded"></p>
                            <p> {!! $post->body !!} <p>
                        </div>
                    </div>
                    {{--  <p><img src="{{ url('/storage/'.$post->image) }}" alt="Post Image" class="img-responsive img-rounded"></p>
                    <p> {!! $post->body !!} <p>  --}}
                    <div class="fh5co-spacer fh5co-spacer-xxs"></div>
                </div>
                {{-- end content --}}
                
                {{-- start sidebar --}}
                <div class="col-md-4">
                    <div class="fh5co-sidebox">
                        <h3 class="fh5co-sidebox-lead">Yang Baru di BLog {{ config('app.name','Laravel') }}</h3>	
                        <ul class="fh5co-post">
                            @foreach($posts as $item)
                            <li>
                                <a href="{{ url('/blog/'.$item->slug) }}">
                                    <div class="fh5co-post-media"><img src="{{ url('/storage/'.$item->image) }}" alt="Post Image"></div>
                                    <div class="fh5co-post-blurb">
                                        <h4>{{ $item->title }}</h4>
                                        <p>{{ str_limit( $item->excerpt, 50) }}</p>
                                        <span class="fh5co-post-meta">{{ $item->created_at->format('M d, Y') }}</span>
                                    </div>
                                </a>
                            </li>
                            @endforeach
                        </ul>                     
                    </div>

                    <div class="fh5co-sidebox">
                        <h3 class="fh5co-sidebox-lead">Paragraph</h3>	
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, temporibus vitae. Dolores sequi, animi dolorem. Ullam minima laudantium culpa dolorem, nulla doloribus totam obcaecati reprehenderit quasi nam eius autem nihil.</p>
                    </div>

                    <div class="fh5co-sidebox">
                        <h3 class="fh5co-sidebox-lead">Check list</h3>	
                        <ul class="fh5co-list-check">
                            <li>Lorem ipsum dolor sit.</li>
                            <li>Nostrum eveniet animi sint.</li>
                            <li>Dolore eligendi, porro ipsam.</li>
                            <li>Repudiandae voluptate dolorem voluptas.</li>
                            <li>Voluptate cupiditate, est laborum?</li>
                        </ul>
                    </div>
                </div>
                {{-- end sidebar --}}
                
            </div>
        </div>
    </div>
@endsection