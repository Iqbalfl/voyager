<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function author()
    {
        return $this->belongsTo(User::class,'author_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    public static function getNewPost()
    {
        return static::select('id','title','image','excerpt','slug','created_at')
                ->where('status','PUBLISHED')
                ->where('featured',1)->orderBy('created_at','asc')
                ->take(4)->get();
    }
}
