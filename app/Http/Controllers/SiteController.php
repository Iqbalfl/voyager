<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Post;

class SiteController extends Controller
{
    public function index(Request $request)
    {
        $sliders = Slider::all();
        $posts = Post::select('id','title','excerpt','image','slug')
                ->where('status','PUBLISHED')
                ->where('featured',1)->paginate(4);

        return view('index')->with(compact('sliders','posts'));
    }
}
