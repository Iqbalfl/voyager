<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class BlogController extends Controller
{
    public function index()
    {
        $posts = Post::select('id','author_id','title','image','body','slug','created_at')
                ->where('status','PUBLISHED')
                ->where('featured',1)->orderBy('created_at','asc')
                ->paginate(5);

        $newposts = Post::getNewPost();
        
        return view('blog.index')->with(compact('posts','newposts'));
    }

    public function show($slug)
    {
        $post = Post::where('slug',$slug)->where('status','PUBLISHED')->first();

        $posts = Post::getNewPost();
        
        return view('blog.show')->with(compact('post','posts'));
    }
}
