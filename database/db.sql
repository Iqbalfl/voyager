-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for voyager
DROP DATABASE IF EXISTS `voyager`;
CREATE DATABASE IF NOT EXISTS `voyager` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `voyager`;

-- Dumping structure for table voyager.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.categories: ~2 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, NULL, 1, 'Category 1', 'category-1', '2017-12-02 17:47:42', '2017-12-02 17:47:42'),
	(2, NULL, 1, 'Category 2', 'category-2', '2017-12-02 17:47:42', '2017-12-02 17:47:42');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table voyager.data_rows
DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE IF NOT EXISTS `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.data_rows: ~67 rows (approximately)
DELETE FROM `data_rows`;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
	(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
	(2, 1, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '', 2),
	(3, 1, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, '', 3),
	(4, 1, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 4),
	(5, 1, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 5),
	(6, 1, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 6),
	(7, 1, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{"resize":{"width":"1000","height":"null"},"quality":"70%","upsize":true,"thumbnails":[{"name":"medium","scale":"50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300","height":"250"}}]}', 7),
	(8, 1, 'slug', 'text', 'slug', 1, 0, 1, 1, 1, 1, '{"slugify":{"origin":"title","forceUpdate":true}}', 8),
	(9, 1, 'meta_description', 'text_area', 'meta_description', 1, 0, 1, 1, 1, 1, '', 9),
	(10, 1, 'meta_keywords', 'text_area', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 10),
	(11, 1, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '{"default":"DRAFT","options":{"PUBLISHED":"published","DRAFT":"draft","PENDING":"pending"}}', 11),
	(12, 1, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '', 12),
	(13, 1, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 13),
	(14, 2, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
	(15, 2, 'author_id', 'text', 'author_id', 1, 0, 0, 0, 0, 0, '', 2),
	(16, 2, 'title', 'text', 'title', 1, 1, 1, 1, 1, 1, '', 3),
	(17, 2, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 4),
	(18, 2, 'body', 'rich_text_box', 'body', 1, 0, 1, 1, 1, 1, '', 5),
	(19, 2, 'slug', 'text', 'slug', 1, 0, 1, 1, 1, 1, '{"slugify":{"origin":"title"}}', 6),
	(20, 2, 'meta_description', 'text', 'meta_description', 1, 0, 1, 1, 1, 1, '', 7),
	(21, 2, 'meta_keywords', 'text', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 8),
	(22, 2, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '{"default":"INACTIVE","options":{"INACTIVE":"INACTIVE","ACTIVE":"ACTIVE"}}', 9),
	(23, 2, 'created_at', 'timestamp', 'created_at', 1, 1, 1, 0, 0, 0, '', 10),
	(24, 2, 'updated_at', 'timestamp', 'updated_at', 1, 0, 0, 0, 0, 0, '', 11),
	(25, 2, 'image', 'image', 'image', 0, 1, 1, 1, 1, 1, '', 12),
	(26, 3, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
	(27, 3, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 2),
	(28, 3, 'email', 'text', 'email', 1, 1, 1, 1, 1, 1, '', 3),
	(29, 3, 'password', 'password', 'password', 0, 0, 0, 1, 1, 0, '', 4),
	(30, 3, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsTo","column":"role_id","key":"id","label":"name","pivot_table":"roles","pivot":"0"}', 10),
	(31, 3, 'remember_token', 'text', 'remember_token', 0, 0, 0, 0, 0, 0, '', 5),
	(32, 3, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '', 6),
	(33, 3, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 7),
	(34, 3, 'avatar', 'image', 'avatar', 0, 1, 1, 1, 1, 1, '', 8),
	(35, 5, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
	(36, 5, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 2),
	(37, 5, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
	(38, 5, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
	(39, 4, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
	(40, 4, 'parent_id', 'select_dropdown', 'parent_id', 0, 0, 1, 1, 1, 1, '{"default":"","null":"","options":{"":"-- None --"},"relationship":{"key":"id","label":"name"}}', 2),
	(41, 4, 'order', 'text', 'order', 1, 1, 1, 1, 1, 1, '{"default":1}', 3),
	(42, 4, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 4),
	(43, 4, 'slug', 'text', 'slug', 1, 1, 1, 1, 1, 1, '{"slugify":{"origin":"name"}}', 5),
	(44, 4, 'created_at', 'timestamp', 'created_at', 0, 0, 1, 0, 0, 0, '', 6),
	(45, 4, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 7),
	(46, 6, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
	(47, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
	(48, 6, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
	(49, 6, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
	(50, 6, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
	(51, 1, 'seo_title', 'text', 'seo_title', 0, 1, 1, 1, 1, 1, '', 14),
	(52, 1, 'featured', 'checkbox', 'featured', 1, 1, 1, 1, 1, 1, '', 15),
	(53, 3, 'role_id', 'text', 'role_id', 1, 1, 1, 1, 1, 1, '', 9),
	(54, 7, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
	(55, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 2),
	(56, 7, 'content', 'text_area', 'Content', 1, 1, 1, 1, 1, 1, NULL, 3),
	(57, 7, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, NULL, 4),
	(58, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
	(59, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
	(60, 8, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
	(61, 8, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
	(62, 8, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{"slugify":{"origin":"title","forceUpdate":true}}', 3),
	(63, 8, 'birth_date', 'date', 'Birth Date', 0, 1, 1, 1, 1, 1, NULL, 4),
	(64, 8, 'birth_place', 'text', 'Birth Place', 0, 1, 1, 1, 1, 1, NULL, 5),
	(65, 8, 'address', 'text', 'Address', 0, 1, 1, 1, 1, 1, NULL, 6),
	(66, 8, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, NULL, 7),
	(67, 8, 'telephone', 'number', 'Telephone', 0, 1, 1, 1, 1, 1, NULL, 8),
	(68, 8, 'socialmedia', 'text', 'Socialmedia', 0, 1, 1, 1, 1, 1, NULL, 9),
	(69, 8, 'description', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, NULL, 10),
	(70, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 11),
	(71, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 12),
	(72, 8, 'photo', 'image', 'Photo', 0, 1, 1, 1, 1, 1, '{"resize":{"width":"1000","height":"null"},"quality":"70%","upsize":true,"thumbnails":[{"name":"medium","scale":"50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300","height":"250"}}]}', 13);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;

-- Dumping structure for table voyager.data_types
DROP TABLE IF EXISTS `data_types`;
CREATE TABLE IF NOT EXISTS `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.data_types: ~6 rows (approximately)
DELETE FROM `data_types`;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `created_at`, `updated_at`) VALUES
	(1, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, '2017-12-02 17:47:35', '2017-12-02 17:47:35'),
	(2, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, '2017-12-02 17:47:35', '2017-12-02 17:47:35'),
	(3, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, '2017-12-02 17:47:35', '2017-12-02 17:47:35'),
	(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, '2017-12-02 17:47:35', '2017-12-02 17:47:35'),
	(5, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, '2017-12-02 17:47:35', '2017-12-02 17:47:35'),
	(6, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, '2017-12-02 17:47:35', '2017-12-02 17:47:35'),
	(7, 'sliders', 'sliders', 'Slider', 'Sliders', 'voyager-tv', 'App\\Slider', NULL, NULL, NULL, 1, 0, '2017-12-02 17:54:22', '2017-12-02 17:54:22'),
	(8, 'teams', 'teams', 'Team', 'Teams', 'voyager-people', 'App\\Team', NULL, NULL, NULL, 1, 0, '2017-12-07 01:21:28', '2017-12-07 01:21:28');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;

-- Dumping structure for table voyager.menus
DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.menus: ~0 rows (approximately)
DELETE FROM `menus`;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', '2017-12-02 17:47:38', '2017-12-02 17:47:38');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Dumping structure for table voyager.menu_items
DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.menu_items: ~13 rows (approximately)
DELETE FROM `menu_items`;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
	(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2017-12-02 17:47:38', '2017-12-02 17:47:38', 'voyager.dashboard', NULL),
	(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2017-12-02 17:47:38', '2017-12-02 17:54:57', 'voyager.media.index', NULL),
	(3, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 5, '2017-12-02 17:47:38', '2017-12-02 17:54:57', 'voyager.posts.index', NULL),
	(4, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2017-12-02 17:47:38', '2017-12-02 17:47:38', 'voyager.users.index', NULL),
	(5, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 7, '2017-12-02 17:47:38', '2017-12-02 17:54:57', 'voyager.categories.index', NULL),
	(6, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 6, '2017-12-02 17:47:38', '2017-12-02 17:54:57', 'voyager.pages.index', NULL),
	(7, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2017-12-02 17:47:38', '2017-12-02 17:47:38', 'voyager.roles.index', NULL),
	(8, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 10, '2017-12-02 17:47:38', '2017-12-07 01:21:54', NULL, NULL),
	(9, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 8, 1, '2017-12-02 17:47:38', '2017-12-02 17:54:57', 'voyager.menus.index', NULL),
	(10, 1, 'Database', '', '_self', 'voyager-data', NULL, 8, 2, '2017-12-02 17:47:38', '2017-12-02 17:54:57', 'voyager.database.index', NULL),
	(11, 1, 'Compass', '/admin/compass', '_self', 'voyager-compass', NULL, 8, 3, '2017-12-02 17:47:38', '2017-12-02 17:54:57', NULL, NULL),
	(12, 1, 'Hooks', '/admin/hooks', '_self', 'voyager-hook', NULL, 8, 4, '2017-12-02 17:47:38', '2017-12-02 17:54:57', NULL, NULL),
	(13, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 11, '2017-12-02 17:47:38', '2017-12-07 01:21:54', 'voyager.settings.index', NULL),
	(14, 1, 'Sliders', '/admin/sliders', '_self', 'voyager-tv', NULL, NULL, 8, '2017-12-02 17:54:22', '2017-12-02 17:54:57', NULL, NULL),
	(15, 1, 'Teams', '/admin/teams', '_self', 'voyager-people', NULL, NULL, 9, '2017-12-07 01:21:28', '2017-12-07 01:21:54', NULL, NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

-- Dumping structure for table voyager.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.migrations: ~23 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(24, '2014_10_12_000000_create_users_table', 1),
	(25, '2014_10_12_100000_create_password_resets_table', 1),
	(26, '2016_01_01_000000_add_voyager_user_fields', 1),
	(27, '2016_01_01_000000_create_data_types_table', 1),
	(28, '2016_01_01_000000_create_pages_table', 1),
	(29, '2016_01_01_000000_create_posts_table', 1),
	(30, '2016_02_15_204651_create_categories_table', 1),
	(31, '2016_05_19_173453_create_menu_table', 1),
	(32, '2016_10_21_190000_create_roles_table', 1),
	(33, '2016_10_21_190000_create_settings_table', 1),
	(34, '2016_11_30_135954_create_permission_table', 1),
	(35, '2016_11_30_141208_create_permission_role_table', 1),
	(36, '2016_12_26_201236_data_types__add__server_side', 1),
	(37, '2017_01_13_000000_add_route_to_menu_items_table', 1),
	(38, '2017_01_14_005015_create_translations_table', 1),
	(39, '2017_01_15_000000_add_permission_group_id_to_permissions_table', 1),
	(40, '2017_01_15_000000_create_permission_groups_table', 1),
	(41, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
	(42, '2017_03_06_000000_add_controller_to_data_types_table', 1),
	(43, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
	(44, '2017_04_21_000000_add_order_to_data_rows_table', 1),
	(45, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
	(46, '2017_08_05_000000_add_group_to_settings_table', 1),
	(47, '2017_12_02_174951_create_sliders_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table voyager.pages
DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.pages: ~0 rows (approximately)
DELETE FROM `pages`;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
	(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2017-12-02 17:47:43', '2017-12-02 17:47:43');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for table voyager.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table voyager.permissions
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.permissions: ~39 rows (approximately)
DELETE FROM `permissions`;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `permission_group_id`) VALUES
	(1, 'browse_admin', NULL, '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(2, 'browse_database', NULL, '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(3, 'browse_media', NULL, '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(4, 'browse_compass', NULL, '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(5, 'browse_menus', 'menus', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(6, 'read_menus', 'menus', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(7, 'edit_menus', 'menus', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(8, 'add_menus', 'menus', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(9, 'delete_menus', 'menus', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(10, 'browse_pages', 'pages', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(11, 'read_pages', 'pages', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(12, 'edit_pages', 'pages', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(13, 'add_pages', 'pages', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(14, 'delete_pages', 'pages', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(15, 'browse_roles', 'roles', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(16, 'read_roles', 'roles', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(17, 'edit_roles', 'roles', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(18, 'add_roles', 'roles', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(19, 'delete_roles', 'roles', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(20, 'browse_users', 'users', '2017-12-02 17:47:39', '2017-12-02 17:47:39', NULL),
	(21, 'read_users', 'users', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(22, 'edit_users', 'users', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(23, 'add_users', 'users', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(24, 'delete_users', 'users', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(25, 'browse_posts', 'posts', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(26, 'read_posts', 'posts', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(27, 'edit_posts', 'posts', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(28, 'add_posts', 'posts', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(29, 'delete_posts', 'posts', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(30, 'browse_categories', 'categories', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(31, 'read_categories', 'categories', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(32, 'edit_categories', 'categories', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(33, 'add_categories', 'categories', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(34, 'delete_categories', 'categories', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(35, 'browse_settings', 'settings', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(36, 'read_settings', 'settings', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(37, 'edit_settings', 'settings', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(38, 'add_settings', 'settings', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(39, 'delete_settings', 'settings', '2017-12-02 17:47:40', '2017-12-02 17:47:40', NULL),
	(40, 'browse_sliders', 'sliders', '2017-12-02 17:54:22', '2017-12-02 17:54:22', NULL),
	(41, 'read_sliders', 'sliders', '2017-12-02 17:54:22', '2017-12-02 17:54:22', NULL),
	(42, 'edit_sliders', 'sliders', '2017-12-02 17:54:22', '2017-12-02 17:54:22', NULL),
	(43, 'add_sliders', 'sliders', '2017-12-02 17:54:22', '2017-12-02 17:54:22', NULL),
	(44, 'delete_sliders', 'sliders', '2017-12-02 17:54:22', '2017-12-02 17:54:22', NULL),
	(45, 'browse_teams', 'teams', '2017-12-07 01:21:28', '2017-12-07 01:21:28', NULL),
	(46, 'read_teams', 'teams', '2017-12-07 01:21:28', '2017-12-07 01:21:28', NULL),
	(47, 'edit_teams', 'teams', '2017-12-07 01:21:28', '2017-12-07 01:21:28', NULL),
	(48, 'add_teams', 'teams', '2017-12-07 01:21:28', '2017-12-07 01:21:28', NULL),
	(49, 'delete_teams', 'teams', '2017-12-07 01:21:28', '2017-12-07 01:21:28', NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table voyager.permission_groups
DROP TABLE IF EXISTS `permission_groups`;
CREATE TABLE IF NOT EXISTS `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.permission_groups: ~0 rows (approximately)
DELETE FROM `permission_groups`;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;

-- Dumping structure for table voyager.permission_role
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.permission_role: ~39 rows (approximately)
DELETE FROM `permission_role`;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1),
	(10, 1),
	(11, 1),
	(12, 1),
	(13, 1),
	(14, 1),
	(15, 1),
	(16, 1),
	(17, 1),
	(18, 1),
	(19, 1),
	(20, 1),
	(21, 1),
	(22, 1),
	(23, 1),
	(24, 1),
	(25, 1),
	(26, 1),
	(27, 1),
	(28, 1),
	(29, 1),
	(30, 1),
	(31, 1),
	(32, 1),
	(33, 1),
	(34, 1),
	(35, 1),
	(36, 1),
	(37, 1),
	(38, 1),
	(39, 1),
	(40, 1),
	(41, 1),
	(42, 1),
	(43, 1),
	(44, 1),
	(45, 1),
	(46, 1),
	(47, 1),
	(48, 1),
	(49, 1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Dumping structure for table voyager.posts
DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.posts: ~4 rows (approximately)
DELETE FROM `posts`;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 'Lorem Ipsum Post', NULL, 'Lorem ipsum dolor sit amet, ei est equidem definitiones. Vix meliore nominavi qualisque te, vim ignota adversarium repudiandae in, erat deleniti salutatus no eam', '<p>Lorem ipsum dolor sit amet, ei est equidem definitiones. Vix meliore nominavi qualisque te, vim ignota adversarium repudiandae in, erat deleniti salutatus no eam. Id nam soleat gubergren appellantur, mei tota hendrerit in. Vis ne vero meis, malis evertitur et has. Animal vivendum periculis id sed, at illum veritus vix. Ut meis propriae democritum nam, mel ex viderer indoctum.</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 1, '2017-12-02 17:47:43', '2017-12-02 17:56:43'),
	(2, 1, 1, 'My Sample Post', NULL, 'Te iusto timeam eam, has dolor disputationi definitionem ut, mei putent eligendi no. Putant forensibus definitionem at vix, quas adhuc ornatus ea vim, ea eam nostrum intellegat necessitatibus.', '<p>Te iusto timeam eam, has dolor disputationi definitionem ut, mei putent eligendi no. Putant forensibus definitionem at vix, quas adhuc ornatus ea vim, ea eam nostrum intellegat necessitatibus. Pri eu nobis noster, et vix accusam reprimique reformidans. An pro minimum accumsan, et usu viris disputationi. Ut pro dicta choro.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 1, '2017-12-02 17:47:43', '2017-12-02 17:57:21'),
	(3, 1, 1, 'Latest Post', NULL, 'At augue consulatu pri, homero nostrum cotidieque usu ea. Maiorum copiosae referrentur ea sed. Ut evertitur percipitur his, cu vim detracto signiferumque', '<p>At augue consulatu pri, homero nostrum cotidieque usu ea. Maiorum copiosae referrentur ea sed. Ut evertitur percipitur his, cu vim detracto signiferumque. Sed quas alienum assentior ex. In sit partiendo urbanitas, his viderer platonem no. Mel omnis percipitur appellantur at, eu affert eripuit moderatius quo.</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 1, '2017-12-02 17:47:43', '2017-12-02 17:57:47'),
	(4, 1, 1, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 1, '2017-12-02 17:47:43', '2017-12-02 17:57:55');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table voyager.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.roles: ~2 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'Administrator', '2017-12-02 17:47:38', '2017-12-02 17:47:38'),
	(2, 'user', 'Normal User', '2017-12-02 17:47:38', '2017-12-02 17:47:38');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table voyager.settings
DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.settings: ~10 rows (approximately)
DELETE FROM `settings`;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
	(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
	(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
	(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
	(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
	(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
	(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
	(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
	(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
	(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
	(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table voyager.sliders
DROP TABLE IF EXISTS `sliders`;
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.sliders: ~2 rows (approximately)
DELETE FROM `sliders`;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` (`id`, `title`, `content`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'Slider 1', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.', 'sliders/December2017/UvkCNS0A1xKP1avgfPz6.jpg', '2017-12-02 18:01:34', '2017-12-02 18:01:34'),
	(2, 'Slider 2', 'Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.', 'sliders/December2017/oTth6BD9hIfVagvYapEG.jpg', '2017-12-02 18:01:49', '2017-12-02 18:01:49');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;

-- Dumping structure for table voyager.teams
DROP TABLE IF EXISTS `teams`;
CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `socialmedia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table voyager.teams: ~0 rows (approximately)
DELETE FROM `teams`;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` (`id`, `name`, `slug`, `birth_date`, `birth_place`, `address`, `email`, `telephone`, `socialmedia`, `description`, `created_at`, `updated_at`, `photo`) VALUES
	(1, 'Muhamad Iqbal', 'iqbal', '1996-01-09', 'Ciamis', 'Bandung', 'muhamad.iqbal46@gmail.com', '089692825280', '@iqbalfl', '<p>Test ini adalah <strong>description</strong></p>', '2017-12-07 01:24:00', '2017-12-07 01:26:36', 'teams/December2017/ZEn0OCwd7hFSKKL5MVvx.jpg');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;

-- Dumping structure for table voyager.translations
DROP TABLE IF EXISTS `translations`;
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.translations: ~30 rows (approximately)
DELETE FROM `translations`;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
	(1, 'data_types', 'display_name_singular', 1, 'pt', 'Post', '2017-12-02 17:47:44', '2017-12-02 17:47:44'),
	(2, 'data_types', 'display_name_singular', 2, 'pt', 'Página', '2017-12-02 17:47:44', '2017-12-02 17:47:44'),
	(3, 'data_types', 'display_name_singular', 3, 'pt', 'Utilizador', '2017-12-02 17:47:44', '2017-12-02 17:47:44'),
	(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2017-12-02 17:47:44', '2017-12-02 17:47:44'),
	(5, 'data_types', 'display_name_singular', 5, 'pt', 'Menu', '2017-12-02 17:47:44', '2017-12-02 17:47:44'),
	(6, 'data_types', 'display_name_singular', 6, 'pt', 'Função', '2017-12-02 17:47:44', '2017-12-02 17:47:44'),
	(7, 'data_types', 'display_name_plural', 1, 'pt', 'Posts', '2017-12-02 17:47:44', '2017-12-02 17:47:44'),
	(8, 'data_types', 'display_name_plural', 2, 'pt', 'Páginas', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(9, 'data_types', 'display_name_plural', 3, 'pt', 'Utilizadores', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(11, 'data_types', 'display_name_plural', 5, 'pt', 'Menus', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(12, 'data_types', 'display_name_plural', 6, 'pt', 'Funções', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2017-12-02 17:47:45', '2017-12-02 17:47:45'),
	(21, 'menu_items', 'title', 2, 'pt', 'Media', '2017-12-02 17:47:46', '2017-12-02 17:47:46'),
	(22, 'menu_items', 'title', 3, 'pt', 'Publicações', '2017-12-02 17:47:46', '2017-12-02 17:47:46'),
	(23, 'menu_items', 'title', 4, 'pt', 'Utilizadores', '2017-12-02 17:47:46', '2017-12-02 17:47:46'),
	(24, 'menu_items', 'title', 5, 'pt', 'Categorias', '2017-12-02 17:47:46', '2017-12-02 17:47:46'),
	(25, 'menu_items', 'title', 6, 'pt', 'Páginas', '2017-12-02 17:47:46', '2017-12-02 17:47:46'),
	(26, 'menu_items', 'title', 7, 'pt', 'Funções', '2017-12-02 17:47:46', '2017-12-02 17:47:46'),
	(27, 'menu_items', 'title', 8, 'pt', 'Ferramentas', '2017-12-02 17:47:46', '2017-12-02 17:47:46'),
	(28, 'menu_items', 'title', 9, 'pt', 'Menus', '2017-12-02 17:47:46', '2017-12-02 17:47:46'),
	(29, 'menu_items', 'title', 10, 'pt', 'Base de dados', '2017-12-02 17:47:46', '2017-12-02 17:47:46'),
	(30, 'menu_items', 'title', 13, 'pt', 'Configurações', '2017-12-02 17:47:46', '2017-12-02 17:47:46');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;

-- Dumping structure for table voyager.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table voyager.users: ~0 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', '$2y$10$7yeOJXmZhJyQobVjOfgIEuKLRvfH5fhZkTxRfE8I2vG4P/YwD/sUS', 'AmUyD7NVsYbAZ5cI9IXTXb73jaclg7MAp42igrpYCnnA3gk6ph5qNVwNU7hF', '2017-12-02 17:47:43', '2017-12-02 17:47:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
